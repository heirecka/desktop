# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require python [ multibuild=false blacklist=3 ]
require vala [ vala_dep=true with_opt=true ]

SUMMARY="A unified framework for many different kinds of real-time communications"
HOMEPAGE="https://telepathy.freedesktop.org/wiki/"
DOWNLOADS="https://telepathy.freedesktop.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="BSD-2 BSD-3 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ description = [ build vala bindings ] requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config[>=0.21]
        gtk-doc? ( dev-doc/gtk-doc[>=1.17] )
    build+run:
        dev-libs/glib:2[>=2.36]
        sys-apps/dbus[>=0.95]
        dev-libs/dbus-glib:1[>=0.90]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30] )
    suggestion:
        net-im/telepathy-gabble[>=0.4.2] [[ description = [ required for jabber accounts ] ]]
        net-im/telepathy-haze            [[ description = [ enables support for all protocols supported by libpurple ] ]]
        net-im/telepathy-idle            [[ description = [ required for IRC connectivity ] ]]
        net-im/telepathy-salut           [[ description = [ required for salut accounts ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/1f15dc935b65a7296dcf815b4da31a3d3b68f116.patch
    "${FILES}"/${PNV}-stop-hardcoding-python-s-path-in-.py-scripts.patch
)
DEFAULT_SRC_CONFIGURE_PARAMS=( '--disable-static' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gtk-doc' 'gobject-introspection introspection'
                                       'vapi vala-bindings' )

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS

    # Use the memory backend instead of dconf. The dconf-settings backend
    # causes problems in our testing environment.
    export GSETTINGS_BACKEND=memory

    esandbox allow_net "unix-abstract:dbus-tube-test"
    esandbox allow_net "unix-abstract:/tmp/dbus-*"
    esandbox allow_net "unix:${TEMP%/}/tp-glib-tests.*/s"
    esandbox allow_net "unix:${TEMP%/}/tp-glib-socket.*/s"
    esandbox allow_net "unix:/tmp/file*"
    esandbox allow_net "inet:0.0.0.0@0"
    esandbox allow_net "inet6:::@0"

    # fails with parallel make, see https://bugs.freedesktop.org/show_bug.cgi?id=96829
    # last checked:  0.24.1
    emake -j1 check

    esandbox disallow_net "unix-abstract:dbus-tube-test"
    esandbox disallow_net "unix-abstract:/tmp/dbus-*"
    esandbox disallow_net "unix:${TEMP%/}/tp-glib-tests.*/s"
    esandbox disallow_net "unix:${TEMP%/}/tp-glib-socket.*/s"
    esandbox disallow_net "unix:/tmp/file*"
    esandbox disallow_net "inet:0.0.0.0@0"
    esandbox disallow_net "inet6:::@0"
}

